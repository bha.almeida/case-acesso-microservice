package br.com.mastertech.consumer;

import br.com.mastertech.acesso.models.AcessoLog;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec4-bruno-henrique-1", groupId = "Bruno A")
    public void receber(@Payload AcessoLog acessoLog) {
//        System.out.println("Recebi o acesso do cliente " + acessoLog.getClienteId() +
//                " na porta " + acessoLog.getPortaId() +
//                " e o acesso é " + acessoLog.isAcessoPermitido());

        try {
            FileWriter fileWriter = new FileWriter("/home/a2w/case-acesso/logAcesso.csv", true);
            PrintWriter printWriter = new PrintWriter(fileWriter);
            StringBuilder sb = new StringBuilder();
            sb.append("Recebi o acesso do cliente ");
            sb.append(acessoLog.getClienteId());
            sb.append(" na porta ");
            sb.append(acessoLog.getPortaId());
            sb.append(" e seu acesso é ");
            if(acessoLog.isAcessoPermitido()){
                sb.append("permitido");
            } else {
                sb.append("negado");
            }
            sb.append('\n');

            printWriter.write(sb.toString());
            printWriter.close();
            System.out.println("salvei arquivo");
        } catch (FileNotFoundException e){
            System.out.println(e.getMessage());
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
}
