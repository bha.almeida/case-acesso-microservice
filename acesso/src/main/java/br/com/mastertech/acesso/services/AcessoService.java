package br.com.mastertech.acesso.services;

import br.com.mastertech.acesso.DTOs.AcessoDTO;
import br.com.mastertech.acesso.client.Cliente;
import br.com.mastertech.acesso.client.ClienteClient;
import br.com.mastertech.acesso.client.Porta;
import br.com.mastertech.acesso.client.PortaClient;
import br.com.mastertech.acesso.converters.AcessoConverter;
import br.com.mastertech.acesso.exceptions.AcessoNaoEncontradoException;
import br.com.mastertech.acesso.models.Acesso;
import br.com.mastertech.acesso.repositories.AcessoRepository;
import net.bytebuddy.asm.Advice;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Random;

@Service
public class AcessoService {

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private PortaClient portaClient;

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private AcessoProducer acessoProducer;

    @Autowired
    private AcessoConverter acessoConverter;


    public Acesso cadastrarAcesso(Acesso acesso){
        Acesso acessoDB = new Acesso();
        Porta porta = portaClient.getPortaById(acesso.getPortaId());
        Cliente cliente = clienteClient.getClienteById(acesso.getClienteId());

        acessoDB.setPortaId(porta.getId());
        acessoDB.setClienteId(cliente.getId());
        acessoRepository.save(acessoDB);

        return acessoDB;
    }

    public Acesso getAcesso(int clienteId, int portaId){
        Porta porta = portaClient.getPortaById(portaId);
        Cliente cliente = clienteClient.getClienteById(clienteId);

        Optional<Acesso> optionalAcesso = acessoRepository.findByPortaIdAndClienteId(porta.getId(),cliente.getId());

        if(!optionalAcesso.isPresent()){
            Acesso acesso = new Acesso();
            acesso.setClienteId(clienteId);
            acesso.setPortaId(portaId);
            acessoProducer.enviarAoKafka(acessoConverter.acessoToAcessoLog(acesso,false));
            throw new AcessoNaoEncontradoException();
        }

        acessoProducer.enviarAoKafka(acessoConverter.acessoToAcessoLog(optionalAcesso.get(), true));
        return optionalAcesso.get();
    }

    @Transactional
    public void deletarAcesso(int clienteId, int portaId){
        Porta porta = portaClient.getPortaById(portaId);
        Cliente cliente = clienteClient.getClienteById(clienteId);

        acessoRepository.removeAllByPortaIdAndClienteId(cliente.getId(), porta.getId());
    }
}
