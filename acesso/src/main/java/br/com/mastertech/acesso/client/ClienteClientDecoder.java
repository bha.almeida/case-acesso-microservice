package br.com.mastertech.acesso.client;

import br.com.mastertech.acesso.exceptions.ClienteNaoEncontradoException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            return new ClienteNaoEncontradoException();
        }
        return errorDecoder.decode(s, response);
    }
}
