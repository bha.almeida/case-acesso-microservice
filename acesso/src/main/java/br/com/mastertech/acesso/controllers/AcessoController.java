package br.com.mastertech.acesso.controllers;

import br.com.mastertech.acesso.DTOs.AcessoDTO;
import br.com.mastertech.acesso.converters.AcessoConverter;
import br.com.mastertech.acesso.models.Acesso;
import br.com.mastertech.acesso.services.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @Autowired
    private AcessoConverter converter;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AcessoDTO cadastrarAcesso(@RequestBody AcessoDTO acessoDTO){
        Acesso acesso = converter.acessoDTOToAcesso(acessoDTO);

        Acesso acessoCadastrado = acessoService.cadastrarAcesso(acesso);

        //acessoService.enviarAoKafka(acessoCadastrado);

        return converter.acessoToAcessoDTO(acessoCadastrado);
    }

    @GetMapping("/{cliente_id}/{porta_id}")
    public AcessoDTO getAcesso(@PathVariable(name = "cliente_id") int clienteId,
                               @PathVariable(name = "porta_id") int portaId){

        Acesso acesso = acessoService.getAcesso(clienteId, portaId);

        return converter.acessoToAcessoDTO(acesso);
    }

    @DeleteMapping("/{cliente_id}/{porta_id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarAcesso(@PathVariable(name = "cliente_id") int clienteId,
                              @PathVariable(name = "porta_id") int portaId){
        acessoService.deletarAcesso(clienteId, portaId);
    }
}
