package br.com.mastertech.acesso.client;

public class PortaClientFallback implements PortaClient {

    @Override
    public Porta getPortaById(int id) {
        Porta porta = new Porta();
        porta.setId(99);
        return porta;
    }
}
