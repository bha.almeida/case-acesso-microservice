package br.com.mastertech.acesso.repositories;

import br.com.mastertech.acesso.models.Acesso;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AcessoRepository extends CrudRepository<Acesso, Integer> {

    Optional<Acesso> findByPortaIdAndClienteId(int portaId, int clienteId);

    void removeAllByPortaIdAndClienteId( int clienteId, int portaId);
}
