package br.com.mastertech.acesso.converters;

import br.com.mastertech.acesso.DTOs.AcessoDTO;
import br.com.mastertech.acesso.models.Acesso;
import br.com.mastertech.acesso.models.AcessoLog;
import org.springframework.stereotype.Component;

@Component
public class AcessoConverter {

    public AcessoDTO acessoToAcessoDTO(Acesso acesso){
        AcessoDTO acessoDTO = new AcessoDTO();
        acessoDTO.setPorta_id(acesso.getPortaId());
        acessoDTO.setCliente_id(acesso.getClienteId());

        return acessoDTO;
    }

    public Acesso acessoDTOToAcesso(AcessoDTO acessoDTO){
        Acesso acesso = new Acesso();
        acesso.setPortaId(acessoDTO.getPorta_id());
        acesso.setClienteId(acessoDTO.getCliente_id());

        return acesso;
    }

    public AcessoLog acessoToAcessoLog(Acesso acesso, boolean acessoPermitido){
        AcessoLog acessoLog = new AcessoLog();
        acessoLog.setClienteId(acesso.getClienteId());
        acessoLog.setPortaId(acesso.getPortaId());
        acessoLog.setAcessoPermitido(acessoPermitido);
        return acessoLog;
    }
}
