package br.com.mastertech.acesso.services;

import br.com.mastertech.acesso.models.AcessoLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AcessoProducer {

    @Autowired
    private KafkaTemplate<String, AcessoLog> producer;

    public void enviarAoKafka(AcessoLog acessoLog) {
        producer.send("spec4-bruno-henrique-1", acessoLog);
    }
}
