package br.com.mastertech.porta.services;

import br.com.mastertech.porta.exceptions.PortaNaoEncontradaException;
import br.com.mastertech.porta.models.Porta;
import br.com.mastertech.porta.repositories.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta cadastrarPorta(Porta porta){
        return portaRepository.save(porta);
    }

    public Porta getPorta(int id){
        Optional<Porta> optionalPorta = portaRepository.findById(id);

        if(!optionalPorta.isPresent()){
            throw new PortaNaoEncontradaException();
        }

        return optionalPorta.get();
    }
}
