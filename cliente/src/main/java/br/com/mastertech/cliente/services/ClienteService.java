package br.com.mastertech.cliente.services;

import br.com.mastertech.cliente.exceptions.ClienteNaoEncontradoException;
import br.com.mastertech.cliente.models.Cliente;
import br.com.mastertech.cliente.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente cadastrarCliente(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente getCliente(int id){
        Optional<Cliente> optionalCliente = clienteRepository.findById(id);

        if(!optionalCliente.isPresent()){
            throw new ClienteNaoEncontradoException();
        }

        return optionalCliente.get();
    }
}
