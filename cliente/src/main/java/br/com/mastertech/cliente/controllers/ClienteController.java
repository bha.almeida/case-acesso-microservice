package br.com.mastertech.cliente.controllers;

import br.com.mastertech.cliente.models.Cliente;
import br.com.mastertech.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente cadastrarCliente(@RequestBody Cliente cliente){
        return clienteService.cadastrarCliente(cliente);
    }

    @GetMapping("/{id}")
    public Cliente getCliente(@PathVariable(name = "id") int id){
        return clienteService.getCliente(id);
    }
}
